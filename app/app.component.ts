import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html'
})
export class AppComponent {
    //[] -> Property Bindings. One Way Up Component to DOM C DOM
    //() -> Events Bindings. One way Down D C
    name = 'Nehal Damania';
    color = 'blue';
    fnChangeColor = function () {
        this.color = (this.color === 'blue') ? 'red' : 'blue';
    }
 }
